# README #

### What is this repository for? ###

This is the inital version for Project Julian, the Electric Skateboard.

### How do I get set up? ###

Pretty easy, install the apk file or open the android project and debug to an android device.
Then go to settings and pair the phone with the HC-05(Or something similar) this is the bluetooth dongle.
Then just open the app, simple!

If for some reason it does not work, confirm that you are using the correct MAC adress of the bluetooth dongle, you can edit this in the settings.

### -Physical Bluetooth controller. ### 
Shouldnt be too hard because most of the heavly lifting code wise is done on the board. Just buy another bluetooth module, (or use that old one) and a ardunino [(Maybe use a lillypad?)](https://www.arduino.cc/en/Main/Boards)  You can even use a Vex Battery, and a 3d printed case.Then just send the value to the board, based on some sort of sensor.

###  Power Assisted Board? ### 
Think a bike system where you add power instead of "setting" it. This way it could act like a normal board until you got tired of pushing where you could just use the motor to maintain your speed.

### Who do I talk to? ###
This project was originaly created by Bryce Cole in 2016.
Please feel free to contact me any time of the day, whenever.

(850)-509-7018

brycewcole@gmail.com