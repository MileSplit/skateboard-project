#include <Servo.h>
#include <SoftwareSerial.h>

  // include the library code:

bool noDataFlag = false;
char command;
String string;
boolean ledon = false;
double rP = 0.0; //The value that is sent from the android, "Received Value"
double aP = 0.0; //The value after we calaculate all the cool stuff.
double oldAP = 0.0; //The previous AP value.
int value = 0; // set values you need to zero

int test = 0;
Servo esc;
int throttlePin = 2;
int bluStatePin = 8;

SoftwareSerial portOne(0,1);

  void setup()
  {
    pinMode(bluStatePin, INPUT);
    pinMode(12, OUTPUT);
    portOne.begin(9600);
    Serial.begin(19200);    // start serial at 9600 baud FOr bluetooth

    //ESC STUFF!
    esc.attach(9);          // attached to pin 9 I just do this with 1 Serv
  }

  void loop()
  {
    if (portOne.available() > 0)
    {
      string = "";
    }

    
    while(portOne.available() > 0)
    {
      command = ((byte)portOne.read());
      
      if(command == ':')
      {
        break;
      }
      
      else
      {
        string += command;
      }
      
      delay(1);
    }
    //Temp Fix for negitve values
    if(rP < 0) rP =0;

    //Check for E-Brake
    if(digitalRead(bluStatePin) == LOW)
    {
      rP = 0;
      tone(12,800);
    }
    else
    {
      noTone(12);
    }
      
    //Apply power
    if(string.length() > 0)
    {
      Serial.println(rP);
      rP = string.toFloat();
    }
    esc.write(rP * 180); //Becauase you always want it to be the rP
    string = "";
 }

