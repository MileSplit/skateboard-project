package com.example.bryce.cruiser;

import android.app.ActionBar;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.w3c.dom.Text;

import static com.example.bryce.cruiser.R.id.macAdressChangeButton;
import static com.example.bryce.cruiser.R.id.macAdressChangeText;

public class Settings extends AppCompatActivity implements View.OnClickListener{
    public static final String PREFS_NAME = "MyPrefsFile";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Settings");
        final SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        final SharedPreferences.Editor editor = settings.edit();
        final EditText mEdit = (EditText)findViewById(R.id.macAdressChangeText);
        Button changeMac = (Button)findViewById(R.id.macAdressChangeButton);
        changeMac.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                editor.putString("DefaultMAC", mEdit.getText().toString());
                editor.apply();
                Toast.makeText(getApplicationContext(), "MAC Address Updated", Toast.LENGTH_LONG).show();

            }
        });
        SharedPreferences settingsPREF = getSharedPreferences(PREFS_NAME, 0);
        mEdit.setText(settingsPREF.getString("DefaultMAC","20:16:08:04:96:21"));
    }

    @Override
    public void onBackPressed() {
        Intent returnHome = new Intent(Settings.this,MainActivity.class);
        startActivity(returnHome);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                this.onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {

    }
}
