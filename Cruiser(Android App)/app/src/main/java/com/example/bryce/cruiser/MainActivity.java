package com.example.bryce.cruiser;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {


    String address = null;
    public static final String PREFS_NAME = "MyPrefsFile";
    private ProgressDialog progress;
    BluetoothAdapter myBluetooth = null;
    BluetoothSocket btSocket = null;
    private boolean isBtConnected = false;
    //SPP UUID. Look for it
    static final UUID myUUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    public static int widthPixels = 0;
    public static int hieghtPixels = 0;
    public final float MAX_DRAG = 0.6f;
    Vibrator v;

    //Recieve info
    TextView bluetoothInputDebug;
    InputStream mmInputStream;
    Thread workerThread;
    byte[] readBuffer;
    int readBufferPosition;
    int counter;
    volatile boolean stopWorker;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        msg("OnCreateWasCAlled");
        Intent newint = getIntent();
        //address = newint.getStringExtra(AutoLoad.EXTRA_ADDRESS); //receive the address of the bluetooth device
        SharedPreferences settings = getSharedPreferences(PREFS_NAME, 0);
        address = settings.getString("DefaultMAC","20:16:08:04:96:21");
        bluetoothInputDebug = (TextView)findViewById(R.id.bluetoothInput);

        //view of the ledControl
        setContentView(R.layout.activity_main);
        v = (Vibrator) MainActivity.this.getSystemService(Context.VIBRATOR_SERVICE);
        myBluetooth = BluetoothAdapter.getDefaultAdapter();

        if(myBluetooth == null)
        {
            //Show a mensag. that the device has no bluetooth adapter
            Toast.makeText(getApplicationContext(), "Bluetooth Device Not Available", Toast.LENGTH_LONG).show();

            //finish apk
            finish();
        }
        else if(!myBluetooth.isEnabled())
        {
            //Ask to the user turn the bluetooth on
            Intent turnBTon = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(turnBTon,1);
        }

        new ConnectBT().execute(); //Call the class to connect

        DisplayMetrics displayMetrics = new DisplayMetrics();
        WindowManager wm = (WindowManager) getApplicationContext().getSystemService(Context.WINDOW_SERVICE); // the results will be higher than using the activity context object or the getWindowManager() shortcut
        wm.getDefaultDisplay().getMetrics(displayMetrics);
        widthPixels = displayMetrics.widthPixels;
        hieghtPixels = displayMetrics.heightPixels;
    }

    // fast way to call Toast
    private void msg(String s)
    {
        Toast.makeText(getApplicationContext(),s,Toast.LENGTH_LONG).show();
    }

    private double baseY = 0.0f;
    private double lastPercent = 0.0;

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        // MotionEvent reports input details from the touch screen
        // and other input controls. In this case, you are only
        // interested in events where the touch position changed.

        switch (e.getAction()) {
            case MotionEvent.ACTION_DOWN:
                baseY = e.getY();
                break;
            case MotionEvent.ACTION_UP:
                if (btSocket!=null) {
                    try {
                        btSocket.getOutputStream().write((0 + ":").getBytes());
                    } catch (IOException i) {
                        msg("Error " + i);
                    }
                }
                break;

            case MotionEvent.ACTION_MOVE:

                double dy = baseY - e.getY();

                double percent = (dy / (hieghtPixels * MAX_DRAG));
                if(percent > 1) percent = 1;
                else if(percent < -1) percent = -1;
                else percent = Math.round(percent * 100.0) / 100.0;
                TextView tv = (TextView)findViewById(R.id.percent);
                tv.setText(Double.toString(Math.round(percent * 100)) + '%');
                if (btSocket!=null) {
                    try {

                        // Vibrate for 500 milliseconds

                        v.vibrate((int)(Math.abs(percent -lastPercent) * 100));
                        btSocket.getOutputStream().write((Double.toString(percent) + ":").getBytes());
                    } catch (IOException i) {
                        msg("Error " + i);
                    }
                }
                else
                {
                    msg("Bluetooth Socket is null");
                }
                lastPercent = percent;
                break;
        }
        return true;
    }

    private class ConnectBT extends AsyncTask<Void, Void, Void>  // UI thread
    {
        private boolean ConnectSuccess = true; //if it's here, it's almost connected

        @Override
        protected void onPreExecute()
        {
            progress = ProgressDialog.show(MainActivity.this, "Connecting...", "Please wait!!!");  //show a progress dialog
        }

        @Override
        protected Void doInBackground(Void... devices) //while the progress dialog is shown, the connection is done in background
        {
            try
            {
                if (btSocket == null || !isBtConnected)
                {
                    myBluetooth = BluetoothAdapter.getDefaultAdapter();//get the mobile bluetooth device
                    BluetoothDevice dispositivo = myBluetooth.getRemoteDevice(address);//connects to the device's address and checks if it's available
                    btSocket = dispositivo.createInsecureRfcommSocketToServiceRecord(myUUID);//create a RFCOMM (SPP) connection
                    BluetoothAdapter.getDefaultAdapter().cancelDiscovery();
                    btSocket.connect();//start connection
                }
            }
            catch (IOException e)
            {
                ConnectSuccess = false;//if the try failed, you can check the exception here
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void result) //after the doInBackground, it checks if everything went fine
        {
            super.onPostExecute(result);

            if (!ConnectSuccess)
            {
                AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                alertDialog.setTitle("Cannot Find Bluetooth Module.");
                alertDialog.setMessage("Most likely its because either the module is not on, you have not yet paired the module with the phone. Do this through the android settings. If the bluetooth module has been changed for some reason, go to settings to change the default MAC address. For more help, contact Bryce at (850)-509-7018.");
                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                //Dismiss it and then restart the app.
                                dialog.dismiss();
                                Intent settingsIntent = new Intent(MainActivity.this,MainActivity.class);
                                Disconnect();

                                startActivity(settingsIntent);
                            }
                        });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "Settings",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                //Dismiss it and then restart the app.
                                dialog.dismiss();
                                Intent settingsIntent = new Intent(MainActivity.this,Settings.class);
                                Disconnect();
                                startActivity(settingsIntent);
                            }
                        });
                alertDialog.show();
            }
            else
            {
                msg("Connected.");
                isBtConnected = true;
                //beginListenForData();
            }
            progress.dismiss();
        }
    }

    public void beginListenForData()
    {
        final Handler handler = new Handler();
        final byte delimiter = 58; //This is the ASCII code for a newline character

        stopWorker = false;
        readBufferPosition = 0;
        readBuffer = new byte[1024];
        try {
            mmInputStream = btSocket.getInputStream();
        }
        catch(Exception e) {

        }
        workerThread = new Thread(new Runnable()
        {
            public void run()
            {
                while(!Thread.currentThread().isInterrupted() && !stopWorker)
                {
                    try
                    {
                        int bytesAvailable = mmInputStream.available();
                        if(bytesAvailable > 0)
                        {
                            byte[] packetBytes = new byte[bytesAvailable];
                            mmInputStream.read(packetBytes);
                            for(int i=0;i<bytesAvailable;i++)
                            {
                                byte b = packetBytes[i];
                                if(b == delimiter)
                                {
                                    byte[] encodedBytes = new byte[readBufferPosition];
                                    System.arraycopy(readBuffer, 0, encodedBytes, 0, encodedBytes.length);
                                    final String data = new String(encodedBytes, "US-ASCII");
                                    readBufferPosition = 0;

                                    handler.post(new Runnable()
                                    {
                                        public void run()
                                        {
                                            bluetoothInputDebug.setText(data);
                                            Toast.makeText(getApplicationContext(), "Bluetooth Device Not Available", Toast.LENGTH_LONG).show();
                                        }
                                    });
                                }
                                else
                                {
                                    readBuffer[readBufferPosition++] = b;
                                }
                            }
                        }
                    }
                    catch (IOException ex)
                    {
                        stopWorker = true;
                    }
                }
            }
        });

        workerThread.start();
    }


    //disconnects all the bluetooth stuff
    private void Disconnect()
    {
        if (btSocket!=null) //If the btSocket is busy
        {
            try
            {
                btSocket.close(); //close connection
            }
            catch (IOException e)
            { msg("Error");}
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case R.id.activity_settings:
                Intent gtsi = new Intent(MainActivity.this , Settings.class);
                Disconnect();
                startActivity(gtsi);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
